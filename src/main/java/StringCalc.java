public class StringCalc {
    private int sum;
    private String temp, str;
    private StringBuilder sb;

    StringCalc() {
        sum = 0;
        temp = "";
        str = "";
        sb = new StringBuilder();
    }

    public int add(String numbers) {
        String[] str = parser(numbers);
        for (String s : str) {

            if (s.equals("")) {
                sb.append("0");
            }
            else if (s.matches("-[0-9]+")) {
                throw new NumberFormatException("negatives not allowed");
            }
            for (char c : s.toCharArray()) {
                if (Character.isDigit(c)) {
                    sb.append(c);
                }
                else if (Character.valueOf(c).equals('/')) {
                    sb.append("0");
                }
            }
            temp = sb.toString();
            sum = sum + Integer.parseInt(temp);
            sb.setLength(0);
        }
        return sum;
    }

    public String[] parser(String str) {
        String delims = "[,\n;]+";
        return str.split(delims);
    }

}
