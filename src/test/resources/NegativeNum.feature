Feature: NegativeNum
    Tests a negative number input.

Scenario: Negative input
    Given I have a calculator
    When I insert a negative number
    Then my calculator should throw an error