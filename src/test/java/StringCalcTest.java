import org.junit.Test;

import static org.junit.Assert.*;

public class StringCalcTest {

    @Test
    public void add1() {
        StringCalc sc = new StringCalc();
        int expected = 0;
        int actual = sc.add("");
        assertEquals(expected, actual);
    }

    @Test
    public void add2() {
        StringCalc sc = new StringCalc();
        int expected = 1;
        int actual = sc.add("1");
        assertEquals(expected, actual);
    }

    @Test
    public void add3() {
        StringCalc sc = new StringCalc();
        int expected = 3;
        int actual = sc.add("1,2");
        assertEquals(expected, actual);
    }

    @Test
    public void addUnknownNumbers() {
        StringCalc sc = new StringCalc();
        int expected = 55;
        int actual = sc.add("1,2,3,4,5,6,7,8,9,10");
        assertEquals(expected, actual);
    }

    @Test
    public void addNewLine() {
        StringCalc sc = new StringCalc();
        int expected = 6;
        int actual = sc.add("1\n2,3");
        assertEquals(expected, actual);
    }

    @Test
    public void semiColon() {
        StringCalc sc = new StringCalc();
        int expected = 3;
        int actual = sc.add("//;\\n1;2");
        assertEquals(expected, actual);
    }

    @Test(expected = NumberFormatException.class)
    public void negativeNumber() {
        StringCalc sc = new StringCalc();
        sc.add("1,-2");
    }
}