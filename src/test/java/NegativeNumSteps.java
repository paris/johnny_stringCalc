import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class NegativeNumSteps {
    StringCalc c;

    @Given("^I have a calculator$")
    public void i_have_a_calculator() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        c = new StringCalc();
    }

    @When("^I insert a negative number$")
    public void i_insert_a_negative_number() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        c.add("1,-2");
    }

    @Then("^my calculator should throw an error$")
    public void my_calculator_should_throw_an_error() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertEquals("NumberFormatException", NumberFormatException.class.getName());
    }
}
